import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'unsplash-gallrey';
  public gallreylist = [
    {
      name: "Dustin Humes",
      img: "../assets/images/lap.jpg",
      id:1
    },
    {
      name: "Davide Manzini",
      img: "../assets/images/alone.jpg",
      id:2
    },
    {
      name: "Peter Chirkov",
      img: "../assets/images/gold.jpg",
      id:3
    },
    {
      name: "Maeva Vigier",
      img: "../assets/images/hand.jpg",
      id:4
    },
    {
      name: "William King",
      img: "../assets/images/walk.jpg",
      id:5
    },
    {
      name: "Jocelyn Morales",
      img: "../assets/images/pillow.jpg",
      id:6
    },
   
    {
      name: "Mailchimp",
      img: "../assets/images/laptop.jpg",
      id:7
    },
    {
      name: "Josiah Gibbs",
      img: "../assets/images/headset.jpg",
      id:8
    },
    {
      name: "mana5280",
      img: "../assets/images/malai.jpg",
      id:9
    },
    {
      name: "Sabina Sturzu",
      img: "../assets/images/girl.jpg",
      id:10
    },
    {
      name: " Dillon Wanner",
      img: "../assets/images/white.jpg",
      id:11
    },
    {
      name: "christian buehner",
      img: "../assets/images/fruit.jpg",
      id:12
    },
    {
      name: "Josiah Gibbs",
      img: "../assets/images/king.jpg",
      id:13
    },
    {
      name: "Milan Csizmadia",
      img: "../assets/images/buliding.jpg",
      id:14
    },
    {
      name: "Maeva Vigier",
      img: "../assets/images/for.jpg",
      id:15
    },
    {
      name: "Klara Kulikova",
      img: "../assets/images/black.jpg",
      id:16
    },
    {
      name: "XPS",
      img: "../assets/images/li.jpg",
      id:17
    },
    {
      name: "hesam jr",
      img: "../assets/images/love.jpg",
      id:18
    },
    {
      name: "Laurence Ziegler",
      img: "../assets/images/tab.jpg",
      id:19
    },
    {
      name: "mana5280",
      img: "../assets/images/beard.jpg",
      id:20
    },
    {
      name: "Yuliia Tretynychenko",
      img: "../assets/images/no.jpg",
      id:21
    },
    {
      name: "Shushan Meloyan",
      img: "../assets/images/dslr.jpg",
      id:22
    },
    {
      name: "Djordje Vukojicic",
      img: "../assets/images/ant.jpg",
      id:23
    },
    {
      name: "Luis Gherasim",
      img: "../assets/images/cy.jpg",
      id:24
    },
    {
      name: "Max Zhang",
      img: "../assets/images/jap.jpg",
      id:25
    },
    {
      name: "Sergey Vinogradov",
      img: "../assets/images/rol.jpg",
      id:26
    },
    {
      name: "Michelangelo Azzariti",
      img: "../assets/images/man.jpg",
      id:27
    },
    {
      name: "Michelangelo Azzariti",
      img: "../assets/images/radio.jpg",
      id:28

    },
  ]
  openModalWindow: boolean = false;
  isShown: boolean = false; // hidden by default
  public modalImg = ''
  public modalname = ''
  public modalid =0
  constructor() { }

  ngOnInit() {
  }


  toggleImageOpen(img: any,name: string,id: number) {
    this.isShown = !this.isShown;
    if (this.isShown) {
      this.modalImg = img;
      this.modalname = name;
      this.modalid = id;
    } else {
      this.modalImg = ''
    }
  }
  minusSlide(n:number) {
    if (n===0){
     n= this.gallreylist.length
    }
    this.modalImg=this.gallreylist[n-1].img;
    this.modalid = n-1
    
 }
 plusSlides(n:number) {
   if 
   (n===this.gallreylist.length){
     n=0
   }
 this.modalImg=this.gallreylist[n+1].img;
 this.modalid = n+1
   }

  



}